package com.arian.bagh_keshavarzi

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ReportAdapter(private val context:Context,
                    private var reportList:ArrayList<Report>)
    : RecyclerView.Adapter<ReportAdapter.MyViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    return MyViewHolder(LayoutInflater.from(parent.context).
            inflate(R.layout.report_card,parent,false))
    }

    override fun getItemCount(): Int {
        return reportList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val report = reportList[position]
        holder.tvTitle?.text = report.title
        holder.tvText?.text = report.text
    }

    inner class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var tvTitle:TextView? = null
        var tvText:TextView? = null

        init {
            tvText = itemView?.findViewById(R.id.tvText)
            tvTitle = itemView?.findViewById(R.id.tvTitle)
        }
    }

}