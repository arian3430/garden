package com.arian.bagh_keshavarzi

data class Report(var id:Long , var title:String? = "" ,
                  var text:String? = "") : Any()