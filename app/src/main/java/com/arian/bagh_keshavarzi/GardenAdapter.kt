package com.arian.bagh_keshavarzi

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList
import android.R.attr.data



class GardenAdapter (private val context: Context, private var gardenList: ArrayList<Garden>) : RecyclerView.Adapter<GardenAdapter.MyViewHolder>(){




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.garden_card,parent,false))
    }

    private var mListener: OnItemClickListener? = null
    private var eListener: OnItemClickListener? = null



    override fun onBindViewHolder(holder: GardenAdapter.MyViewHolder, position: Int) {
        val garden = gardenList[position]
        holder.tvName?.text = garden.name
        holder.tvNumber?.text = garden.number

    }



    override fun getItemCount(): Int {
        return gardenList.size
    }


    interface OnItemClickListener {
        fun onDeleteClick(position: Int)
    }


    interface OnItemClickListenerEdit : OnItemClickListener {
        fun onEditClick(position: Int)
    }


    fun setOnClickListenerEdit(listenerEdite: OnItemClickListenerEdit) {
        eListener = listenerEdite
    }

    fun setOnClickListener(listener: OnItemClickListener) {
        mListener = listener
    }


    fun setfilter(listitem: ArrayList<Garden>): ArrayList<Garden>? {
        gardenList!!.clear()
        /*mFilteredList = ArrayList()*/
        gardenList!!.addAll(listitem)
        notifyDataSetChanged()
        return gardenList
    }

    inner class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {



        var tvName: TextView? = null
        var tvNumber: TextView? = null
        var imageView:ImageView = itemView!!.findViewById(R.id.delete)
        var ivEdit:ImageView = itemView!!.findViewById(R.id.edit)


        init {

            imageView.setOnClickListener{

                if (mListener != null) {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        mListener!!.onDeleteClick(position)
                    }
                }

            }
        }

        init {

            ivEdit.setOnClickListener{

                if (eListener != null) {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        eListener!!.onDeleteClick(position)
                    }
                }

            }
        }


        init {
            tvName = itemView?.findViewById(R.id.tvName)
            tvNumber = itemView?.findViewById(R.id.tvNumber)
        }

    }


}