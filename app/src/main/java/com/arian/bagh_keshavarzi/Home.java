package com.arian.bagh_keshavarzi;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.viewpagerindicator.CirclePageIndicator;
import java.util.Timer;
import java.util.TimerTask;


public class Home extends Fragment {

    public Home() {
        //  empty public constructor
    }
    Button button,btn1,btnT,btnE;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);


        button = (Button) view.findViewById(R.id.btn);
        btn1 = (Button) view.findViewById(R.id.btn2);
        btnT = (Button) view.findViewById(R.id.btnT);
        btnE = (Button) view.findViewById(R.id.btnE);


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent( getActivity(), GardenSaveActivity.class);
                startActivity(intent);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent( getActivity(), ReportActivity.class);
                startActivity(intent);
            }
        });

        btnE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri uri = Uri.parse("https://www.instagram.com/Seyed_m_torabi/");
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                likeIng.setPackage("com.instagram.android");

                try {
                    startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.instagram.com/Seyed_m_torabi/")));
                }

            }
        });


        btnT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent telegram = new Intent(Intent.ACTION_VIEW , Uri.parse("https://t.me/saeidpoorebadi"));
                startActivity(telegram);

            }
        });

        return view;
    }




    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    }

