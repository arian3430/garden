package com.arian.bagh_keshavarzi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_report.*
import android.widget.LinearLayout
import android.widget.EditText
import android.widget.LinearLayout.VERTICAL
import java.util.*


class ReportActivity : AppCompatActivity() {
    private var list = ArrayList<Report>()

    private lateinit var sharedDB: SharedDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)
        sharedDB = SharedDB(this)

        rvReport.layoutManager = LinearLayoutManager(this)
        list = (sharedDB.getListObject("Key", Report::class.java) as ArrayList<Report>)
        val adapter = ReportAdapter(this, list)
        rvReport.adapter = adapter




        fabAddReport.setOnClickListener {
            val etTitle = EditText(this@ReportActivity)
            val etText = EditText(this@ReportActivity)
            etTitle.hint = "عنوان گزارش را وارد کنید"
            etText.hint = "متن گزارش را وارد کنید"
            val lp = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
            etTitle.layoutParams = lp
            etText.layoutParams = lp

            val linearLayout = LinearLayout(this)
            linearLayout.layoutParams = lp
            linearLayout.orientation = VERTICAL
            linearLayout.addView(etTitle)
            linearLayout.addView(etText)


            AlertDialog.Builder(this@ReportActivity)
                    .setTitle("گزارش خود را ثبت کنید")
                    .setPositiveButton("اضافه کردن"
                    ) { _, _ ->

                        val report = Report(System.currentTimeMillis())
                        etTitle.text?.let {
                            report.title = it.toString()
                        }

                        etText.text?.let {
                            report.text = it.toString()
                        }

                        list.add(report)
                        list.reverse()


                        sharedDB.putListObject("Key",
                                list)



                        adapter.notifyDataSetChanged()
                    }
                    .setNegativeButton("لغو") { _, _ ->

                    }
                    .setView(linearLayout)
                    .show()
        }


    }
}
