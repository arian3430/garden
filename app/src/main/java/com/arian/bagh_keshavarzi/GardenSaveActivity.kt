package com.arian.bagh_keshavarzi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_garden_save.*
import java.util.ArrayList
import android.R.attr.data
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.recyclerview.widget.RecyclerView


class GardenSaveActivity : AppCompatActivity() ,SearchView.OnQueryTextListener {


    private var list = ArrayList<Garden>()

    private lateinit var sharedDB: SharedDBB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_garden_save)
        sharedDB = SharedDBB(this)
        rvSave.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        list = (sharedDB.getListObject("KEY", Garden::class.java) as ArrayList<Garden>)
        val adapter = GardenAdapter(this, list)
        rvSave.adapter = adapter





//        val myButton = findViewById<Button>(R.id.b)
////
//        myButton.setOnClickListener {
//
//            if (list.size != 0) {
//
//                list.removeAt(0)
//                rvSave.removeViewAt(0)
//                adapter.notifyItemRemoved(0)
//                adapter.notifyItemRangeChanged(0, list.size)
//                sharedDB.putListObject("KEY",
//                        list)
//
//            }
//            adapter.notifyDataSetChanged()
//
//        }

        adapter.setOnClickListener(object : GardenAdapter.OnItemClickListener {
            override fun onDeleteClick(position: Int) {

                if (list.size!=0){
                    list.removeAt(position)
                    rvSave.removeViewAt(position)
                    adapter!!.notifyItemRemoved(position)
                    adapter.notifyItemRangeChanged(position,position)
                    sharedDB.putListObject("KEY",
                            list)

                }


            }



        })


        adapter.setOnClickListenerEdit(object : GardenAdapter.OnItemClickListenerEdit{

            override fun onEditClick(position: Int) {

                if (list.size!=0){
                    list.removeAt(position)
                    rvSave.removeViewAt(position)
                    adapter!!.notifyItemRemoved(position)
                    adapter.notifyItemRangeChanged(position,position)
                    sharedDB.putListObject("KEY",
                            list)

                }

                val etName = EditText(this@GardenSaveActivity)
                val etNumber = EditText(this@GardenSaveActivity)
                etName.hint = "نام باغ را وارد کنید"
                etNumber.hint = "شماره باغ را وارد کنید"
                val lp = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                etName.layoutParams = lp
                etNumber.layoutParams = lp

                val linearLayout = LinearLayout(this@GardenSaveActivity)
                linearLayout.layoutParams = lp
                linearLayout.orientation = LinearLayout.VERTICAL
                linearLayout.addView(etName)
                linearLayout.addView(etNumber)


                AlertDialog.Builder(this@GardenSaveActivity)
                        .setTitle("اطلاعات باغ")
                        .setPositiveButton("اضافه کردن"
                        ) { _, _ ->

                            val garden = Garden(System.currentTimeMillis())
                            etName.text?.let {
                                garden.name = it.toString()
                            }

                            etNumber.text?.let {
                                garden.number = it.toString()
                            }

                            list.add(garden)
                            list.reverse()


                            sharedDB.putListObject("KEY",
                                    list)



                            adapter.notifyDataSetChanged()
                        }
                        .setNegativeButton("لغو") { _, _ ->

                        }
                        .setView(linearLayout)
                        .show()

            }

            override fun onDeleteClick(position: Int) {

                if (list.size!=0){
                    list.removeAt(position)
                    rvSave.removeViewAt(position)
                    adapter!!.notifyItemRemoved(position)
                    adapter.notifyItemRangeChanged(position,position)
                    sharedDB.putListObject("KEY",
                            list)

                }

                val etName = EditText(this@GardenSaveActivity)
                val etNumber = EditText(this@GardenSaveActivity)
                etName.hint = "نام باغ را وارد کنید"
                etNumber.hint = "شماره باغ را وارد کنید"
                val lp = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                etName.layoutParams = lp
                etNumber.layoutParams = lp

                val linearLayout = LinearLayout(this@GardenSaveActivity)
                linearLayout.layoutParams = lp
                linearLayout.orientation = LinearLayout.VERTICAL
                linearLayout.addView(etName)
                linearLayout.addView(etNumber)


                AlertDialog.Builder(this@GardenSaveActivity)
                        .setTitle("اطلاعات باغ")
                        .setPositiveButton("اضافه کردن"
                        ) { _, _ ->

                            val garden = Garden(System.currentTimeMillis())
                            etName.text?.let {
                                garden.name = it.toString()
                            }

                            etNumber.text?.let {
                                garden.number = it.toString()
                            }

                            list.add(garden)
                            list.reverse()


                            sharedDB.putListObject("KEY",
                                    list)



                            adapter.notifyDataSetChanged()
                        }
                        .setNegativeButton("لغو") { _, _ ->

                        }
                        .setView(linearLayout)
                        .show()
            }
        })


            fabAddSave.setOnClickListener {
                val etName = EditText(this@GardenSaveActivity)
                val etNumber = EditText(this@GardenSaveActivity)
                etName.hint = "نام باغ را وارد کنید"
                etNumber.hint = "شماره باغ را وارد کنید"
                val lp = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT)
                etName.layoutParams = lp
                etNumber.layoutParams = lp

                val linearLayout = LinearLayout(this)
                linearLayout.layoutParams = lp
                linearLayout.orientation = LinearLayout.VERTICAL
                linearLayout.addView(etName)
                linearLayout.addView(etNumber)


                AlertDialog.Builder(this@GardenSaveActivity)
                        .setTitle("اطلاعات باغ")
                        .setPositiveButton("اضافه کردن"
                        ) { _, _ ->

                            val garden = Garden(System.currentTimeMillis())
                            etName.text?.let {
                                garden.name = it.toString()
                            }

                            etNumber.text?.let {
                                garden.number = it.toString()
                            }

                            list.add(garden)
                            list.reverse()


                            sharedDB.putListObject("KEY",
                                    list)



                            adapter.notifyDataSetChanged()
                        }
                        .setNegativeButton("لغو") { _, _ ->

                        }
                        .setView(linearLayout)
                        .show()
            }



        }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.toolbar_menu,menu)

        val menuItem : MenuItem = menu!!.findItem(R.id.action_search)
        val searchView:SearchView = menuItem.actionView as SearchView
        searchView.setOnQueryTextListener(this)


        return true
    }
    override fun onQueryTextSubmit(p0: String?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onQueryTextChange(newText: String?): Boolean {





        return false

    }

}

