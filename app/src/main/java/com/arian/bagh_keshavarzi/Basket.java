package com.arian.bagh_keshavarzi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.fragment.app.Fragment;



public class Basket extends Fragment {


    public Basket() {
        // Required empty public constructor
    }


    WebView myWebView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_basket, container, false);
        myWebView = view.findViewById(R.id.webView);
        myWebView.loadUrl("https://limooland.com/product-category/nuts/pistachio/");

        return view;


    }

}
