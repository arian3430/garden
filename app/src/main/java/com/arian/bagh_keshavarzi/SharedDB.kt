package com.arian.bagh_keshavarzi

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.text.TextUtils
import com.google.gson.Gson
import java.util.*

class SharedDB(context: Context) {
    private var preferences: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(context)

    fun putListObject(key: String, objArray: ArrayList<out Any>) {
        val gson = Gson()
        val objStrings = ArrayList<String>()
        for (obj in objArray) {
            objStrings.add(gson.toJson(obj))
        }
        putListString(key, objStrings)
    }

    fun getListObject(key: String, mClass: Class<*>): ArrayList<*> {
        val gson = Gson()

        val objStrings = getListString(key)
        val objects = ArrayList<Any>()

        for (jObjString in objStrings) {
            val value = gson.fromJson<Any>(jObjString, mClass)
            objects.add(value)
        }
        return objects
    }

    private fun putListString(key: String, stringList: ArrayList<String>) {
        val myStringList = stringList.toTypedArray()
        preferences.edit().putString(key,
                TextUtils.join("‚‗‚", myStringList)).apply()
    }

    private fun getListString(key: String): ArrayList<String> {
        return ArrayList(Arrays.asList(*TextUtils.split(preferences.getString(key, ""), "‚‗‚")))
    }
}